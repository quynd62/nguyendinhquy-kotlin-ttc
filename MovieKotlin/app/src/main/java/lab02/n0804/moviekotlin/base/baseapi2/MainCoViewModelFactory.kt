package lab02.n0804.moviekotlin.base.baseapi2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MainCoViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainCoViewModel(repository) as T
    }
}