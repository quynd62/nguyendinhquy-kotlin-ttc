package lab02.n0804.moviekotlin.detailFilm.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SpokenLanguage(
        @SerializedName("iso_639_1")
        @Expose
        val iso_639_1:String,
        @SerializedName("name")
        @Expose
        val name:String
)