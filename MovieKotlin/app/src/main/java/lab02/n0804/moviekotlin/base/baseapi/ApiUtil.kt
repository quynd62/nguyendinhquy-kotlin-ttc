package lab02.n0804.moviekotlin.base.baseapi

open class ApiUtil {
    companion object{
        const val BASE_URL:String = "https://api.themoviedb.org/3/"

        fun getDataApi() :ApiRepository?{
            return ApiBuilder.getApi(BASE_URL)?.create(ApiRepository::class.java)
        }
    }
}