package lab02.n0804.moviekotlin.nowplaying.viewModel

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import lab02.n0804.moviekotlin.base.BaseViewModel
import lab02.n0804.moviekotlin.base.baseapi.ApiUtil
import lab02.n0804.moviekotlin.main.Movie
import lab02.n0804.moviekotlin.upcoming.model.MovieResponse
import lab02.n0804.moviekotlin.util.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NowPlayingHomeViewModel : BaseViewModel(){
    private var listNowPlayingMovie : MutableLiveData<List<Movie>> = MutableLiveData()
    private var isCallApi : MutableLiveData<Boolean> = MutableLiveData()

    fun getIsCallApi() : MutableLiveData<Boolean>{
        return isCallApi
    }

    fun setIsCallApi(isCallApi: MutableLiveData<Boolean>) : Unit{
        this.isCallApi = isCallApi
    }

    fun getListNow() : MutableLiveData<List<Movie>>{
        return listNowPlayingMovie
    }

    fun setListNow(listNow : MutableLiveData<List<Movie>>) : Unit{
        this.listNowPlayingMovie = listNow
    }

    fun initDataViewModel() : Unit{
        getListPoPular()
    }

    private fun getListPoPular() {
        //call Api để láy về dữ liệu
        var params : HashMap<String,Any> = hashMapOf<String,Any>()
        params["api_key"] = Utils.API_KEY

        val getlistNowPlayingMovie: Call<MovieResponse> = ApiUtil.getDataApi()!!.getNowPlayingMovies(params as Map<String, String>)
        getlistNowPlayingMovie.enqueue(object : Callback<MovieResponse>{
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                if (response.body() != null && response.body()?.results != null){
                    listNowPlayingMovie.value = response.body()?.results
                }else{
                    Toast.makeText(mActivity,"k co data",Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Toast.makeText(mActivity,"call loi",Toast.LENGTH_SHORT).show()
            }

        })
    }
}