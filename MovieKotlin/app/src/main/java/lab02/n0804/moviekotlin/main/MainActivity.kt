package lab02.n0804.moviekotlin.main

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_2.*
import kotlinx.android.synthetic.main.fragment_popular_home.*
import lab02.n0804.moviekotlin.R
import lab02.n0804.moviekotlin.base.BaseActivity
import lab02.n0804.moviekotlin.base.baseapi2.MainCoViewModel
import lab02.n0804.moviekotlin.base.baseapi2.MainCoViewModelFactory
import lab02.n0804.moviekotlin.base.baseapi2.Repository
import lab02.n0804.moviekotlin.nowplaying.view.NowPlayingHomeFragment
import lab02.n0804.moviekotlin.popular.view.PopularHomeFragment
import lab02.n0804.moviekotlin.toprate.view.TopRateHomeFragment
import lab02.n0804.moviekotlin.upcoming.adapter.ListMovieHorizontalAdapter
import lab02.n0804.moviekotlin.upcoming.view.UpComingHomeFragment

class MainActivity : AppCompatActivity() {
    private lateinit var viewModelCo: MainCoViewModel

    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_upcoming -> {
                    moveToFragment(UpComingHomeFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_toprate -> {
                    moveToFragment(TopRateHomeFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_popular -> {
//                    item.isChecked = false
                    moveToFragment(PopularHomeFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_now_playing -> {
                    moveToFragment(NowPlayingHomeFragment())
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    private fun moveToFragment(fragment: Fragment) {
        val fragmentTrans = supportFragmentManager.beginTransaction()
        fragmentTrans.replace(R.id.fragment_container, fragment)
        fragmentTrans.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main_2)
//        val navView: BottomNavigationView = findViewById(R.id.nav_view)
//
//        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
//        moveToFragment(UpComingHomeFragment())

        rv_movies_list.layoutManager = LinearLayoutManager(this)
        rv_movies_list.setHasFixedSize(true)

        val repository = Repository()
        val viewModelFactory = MainCoViewModelFactory(repository)
        viewModelCo = ViewModelProvider(this,viewModelFactory).get(MainCoViewModel::class.java)
        viewModelCo.getTopRateCoroutine()
        viewModelCo.myResponse.observe(this, Observer { response ->
            Log.d("quynd",response.totalPages.toString())
            Log.d("quynd",response.totalResults.toString())
            Log.d("quynd",response.page.toString())
            Log.d("quynd",response.results.toString())
            rv_movies_list.adapter = response.results?.let { MovieAdapter(it) }
//            if (response.results != null){
////                if (response.results.isNotEmpty()){
////                    if (topRateAdapter == null){
////                        context?.let { it1 ->
////                            topRateAdapter = ListMovieHorizontalAdapter(it1,
////                                response.results as MutableList<Movie>?,object : ListMovieHorizontalAdapter.ClickItemListener{
////                                override fun onClickMovie(position: Int) {
////
////                                }
////                            })
////                        }
////                        rv_popular.adapter = topRateAdapter
////
////
////
////                    }else{
////                        rv_popular.adapter = topRateAdapter
////                    }
////                }
//                topRateAdapter =
//                    context?.let {
//                        ListMovieHorizontalAdapter(it,response.results,object : ListMovieHorizontalAdapter.ClickItemListener{
//                            override fun onClickMovie(position: Int) {
//
//                            }
//                        })
//                    }
//                rv_popular.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
//                rv_popular.adapter = topRateAdapter
//
//            }else{
//                Toast.makeText(getActivity(),"call loi api", Toast.LENGTH_SHORT).show()
//            }
        })
    }

    companion object{
        var ID_FILM:String = "ID_FILM"
    }

    var postionTab = 0
    private var listBottomNavigation:MutableList<AHBottomNavigationItem>? = null
    private var viewModel: MainViewModel? = null
    private var upcomingHomeFragment: UpComingHomeFragment? = null
    private var topRatedHomeFragment: TopRateHomeFragment? = null
    private var popularHomeFragment: PopularHomeFragment? = null
    private var nowPlayingHomeFragment: NowPlayingHomeFragment? = null

    private var searcAdapter: SearchAdapter? = null

//    override fun initActivity() {
////        viewModel = getViewModel(MainViewModel::class.java)
////        initViewModel()
////        val navView: BottomNavigationView = findViewById(R.id.nav_view)
////
////        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
////        moveToFragment(UpComingHomeFragment())
////        bottom_navigation.isBehaviorTranslationEnabled = false
////        bottom_navigation.currentItem = postionTab
////        updateBottomNavigation()
////        initView()
//    }

//    private fun initView() {
//
//    }

//    @SuppressLint("NewApi")
//    private fun updateBottomNavigation() {
//        listBottomNavigation = ArrayList()
//        var item1: AHBottomNavigationItem = AHBottomNavigationItem("Upcoming", R.drawable.ic_upcoming)
//        var item2: AHBottomNavigationItem = AHBottomNavigationItem("Top Rated", R.drawable.ic_top_rated)
//        var item3: AHBottomNavigationItem = AHBottomNavigationItem("Popular", R.drawable.ic_top_rated)
//        var item4: AHBottomNavigationItem = AHBottomNavigationItem("Now Playing", R.drawable.ic_now_playing)
//
//        listBottomNavigation?.add(item1)
//        listBottomNavigation?.add(item2)
//        listBottomNavigation?.add(item3)
//        listBottomNavigation?.add(item4)
//
//        bottom_navigation.accentColor = Color.parseColor("#ffffff")
//        bottom_navigation.inactiveColor = Color.parseColor("#ffffff")
//        bottom_navigation.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
//        bottom_navigation.defaultBackgroundColor = Color.parseColor("#000000")
//
//        bottom_navigation.removeAllItems()
//        bottom_navigation.addItems(listBottomNavigation)
//
//        initViewPager()
//    }

//    private fun initViewPager() : Unit{
//        var fragmentAdapter :MoviesPagerAdapter = MoviesPagerAdapter(supportFragmentManager)
//        setupViewPager(fragmentAdapter)
//
//    }
//
//    private fun setupViewPager(fragmentAdapter: MainActivity.MoviesPagerAdapter) {
//        view_pager_main.adapter = fragmentAdapter
//        view_pager_main.offscreenPageLimit = fragmentAdapter.count
//
//        bottom_navigation.setOnTabSelectedListener(object : AHBottomNavigation.OnTabSelectedListener{
//            override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
//                view_pager_main.currentItem = position
//                return true
//            }
//        })
//
//        view_pager_main.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
//            override fun onPageScrollStateChanged(state: Int) {
//
//            }
//
//            override fun onPageScrolled(
//                position: Int,
//                positionOffset: Float,
//                positionOffsetPixels: Int
//            ) {
//
//            }
//
//            override fun onPageSelected(position: Int) {
//                bottom_navigation.currentItem = position
////                when(position){
////                    0->
////                }
//            }
//        })
//    }

    private fun initViewModel() {
        viewModel?.getListFilmSearch()?.observe(this,Observer<List<Movie>?>{ it ->
            if (it != null){
                if (it.isNotEmpty()){
                    if (searcAdapter == null){
                        SearchAdapter(this,object : SearchAdapter.OnClickListener{
                            override fun onItemClick(position: Int) {

                            }
                        }).setListData(it as MutableList<Movie>?)
                        rv_search.adapter = searcAdapter
                    }else{
                        searcAdapter?.setListData(it as MutableList<Movie>?)
                    }
                }
            }else{
                Toast.makeText(this,"call loi api",Toast.LENGTH_SHORT).show()
            }
        })
    }

//    override fun getLayoutId(): Int{
//        return R.layout.activity_main
//    }
//    inner class MoviesPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager){
//        override fun getCount(): Int {
//            return 4
//        }
//
//        override fun getItem(position: Int): Fragment {
//            var fragment:Fragment? = null
//            when(position){
//                0 -> {
//                    if (upcomingHomeFragment == null)
//                        upcomingHomeFragment = UpComingHomeFragment.getInstance()
//                    fragment = upcomingHomeFragment
//                }
//                1 -> {
//                    if (topRatedHomeFragment == null)
//                        topRatedHomeFragment = TopRateHomeFragment.getInstance()
//                    fragment = topRatedHomeFragment
//                }
//                2 -> {
//                    if (popularHomeFragment == null)
//                        popularHomeFragment = PopularHomeFragment.getInstance()
//                    fragment = popularHomeFragment
//                }
//                3 -> {
//                    if (nowPlayingHomeFragment == null)
//                        nowPlayingHomeFragment = NowPlayingHomeFragment.getInstance()
//                    fragment = nowPlayingHomeFragment
//                }
//
//            }
//            return fragment!!
//        }
//
//    }
}