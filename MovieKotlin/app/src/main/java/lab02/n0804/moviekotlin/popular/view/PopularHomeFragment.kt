package lab02.n0804.moviekotlin.popular.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_now_playing_home.*
import kotlinx.android.synthetic.main.fragment_now_playing_home.rv_now_playing
import kotlinx.android.synthetic.main.fragment_popular_home.*
import lab02.n0804.moviekotlin.R
import lab02.n0804.moviekotlin.base.BaseFragment
import lab02.n0804.moviekotlin.main.Movie
import lab02.n0804.moviekotlin.nowplaying.viewModel.NowPlayingHomeViewModel
import lab02.n0804.moviekotlin.popular.viewmodel.PopularHomeViewModel
import lab02.n0804.moviekotlin.upcoming.adapter.ListMovieHorizontalAdapter

class PopularHomeFragment() : BaseFragment(){
    private var viewModelPopular: PopularHomeViewModel? = null
    private var popularAdapter: ListMovieHorizontalAdapter? = null
    companion object{
        @Volatile var fInstance: PopularHomeFragment? = null
        fun getInstance() : PopularHomeFragment {
            if (fInstance == null){
                fInstance = PopularHomeFragment()
            }
            return fInstance as PopularHomeFragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_popular_home
    }

    override fun initFragment() {
//        initViewModel()
        initView()
    }

    private fun initView() {

    }

    private fun initViewModel() {
        viewModelPopular = getViewModel(PopularHomeViewModel::class.java)
        viewModelPopular?.initDataViewModel()

        viewModelPopular?.getListPop()?.observe(this, Observer<List<Movie>?>{ it ->
            if (it != null){
                if (it.isNotEmpty()){
//                    if (popularAdapter == null){
//                        context?.let { it1 ->
//                            ListMovieHorizontalAdapter(it1,object : ListMovieHorizontalAdapter.ClickItemListener{
//                                override fun onClickMovie(position: Int) {
//
//                                }
//                            })
//                        }
//                        rv_popular.adapter = popularAdapter
//
//                    }else{
//                        rv_popular.adapter = popularAdapter
//                    }
                }
            }else{
                Toast.makeText(getActivity(),"call loi api", Toast.LENGTH_SHORT).show()
            }
        })

    }

}