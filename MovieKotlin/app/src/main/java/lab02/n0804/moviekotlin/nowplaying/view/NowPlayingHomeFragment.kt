package lab02.n0804.moviekotlin.nowplaying.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_now_playing_home.*
import lab02.n0804.moviekotlin.R
import lab02.n0804.moviekotlin.base.BaseFragment
import lab02.n0804.moviekotlin.main.Movie
import lab02.n0804.moviekotlin.main.SearchAdapter
import lab02.n0804.moviekotlin.nowplaying.viewModel.NowPlayingHomeViewModel
import lab02.n0804.moviekotlin.upcoming.adapter.ListMovieHorizontalAdapter

class NowPlayingHomeFragment : BaseFragment() {
    private var viewModel:NowPlayingHomeViewModel? = null
    private var nowPlayingAdapter: ListMovieHorizontalAdapter? = null


    companion object{
        @Volatile var fInstance:NowPlayingHomeFragment? = null
        fun getInstance() : NowPlayingHomeFragment{
            if (fInstance == null){
                fInstance = NowPlayingHomeFragment()
            }
            return fInstance as NowPlayingHomeFragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_now_playing_home
    }

    override fun initFragment() {
//        initViewModel()
        initView()
    }

    override fun onResume() {
        super.onResume()
//        initViewModel()
    }

    private fun initView() {

    }

    private fun initViewModel() {
        viewModel = getViewModel(NowPlayingHomeViewModel::class.java)
        viewModel?.initDataViewModel()

        viewModel?.getListNow()?.observe(this, Observer<List<Movie>?>{ it ->
            if (it != null){
                if (it.isNotEmpty()){
                    if (nowPlayingAdapter == null){
//                        context?.let { it1 ->
//                            ListMovieHorizontalAdapter(it1,object : ListMovieHorizontalAdapter.ClickItemListener{
//                                override fun onClickMovie(position: Int) {
//
//                                }
//                            })
//                        }
//                        rv_now_playing.adapter = nowPlayingAdapter

                    }else{
                        rv_now_playing.adapter = nowPlayingAdapter
                    }
                }
            }else{
                Toast.makeText(getActivity(),"call loi api", Toast.LENGTH_SHORT).show()
            }
        })

//        viewModel?.getIsCallApi()?.observe(this,object :Observer<Boolean>{
//            override fun onChanged(t: Boolean?) {
//
//            }
//        })

    }
}