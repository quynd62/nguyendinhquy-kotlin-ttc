package lab02.n0804.moviekotlin.upcoming.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import lab02.n0804.moviekotlin.R
import lab02.n0804.moviekotlin.main.Movie
import lab02.n0804.moviekotlin.util.Utils

class ListMovieHorizontalAdapter (private val context: Context,
                                  private val listMovie:List<Movie>,
                                  private val mListener:ClickItemListener
) : RecyclerView.Adapter<ListMovieHorizontalAdapter.ViewHolder>() {

//    private var listMovie:MutableList<Movie>? = null

    inner class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemMovieImg : ImageView = itemView.findViewById(R.id.imgMovie)
        var nameTv : TextView = itemView.findViewById(R.id.tvNameMovie)
        var detailTV: TextView = itemView.findViewById(R.id.tvDetail)
    }

    interface ClickItemListener{
        fun onClickMovie(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.movie_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie: Movie = listMovie!![position]
        //xử lý cho holder của search adapter, bắt sự kiện,...
        var urlImg:String  = Utils.KEY_IMAGE + movie.posterPath
        Glide.with(holder.itemMovieImg).load(urlImg).into(holder.itemMovieImg)
        holder.nameTv.text = movie.title
        holder.detailTV.text = movie.overview
        holder.itemMovieImg.setOnClickListener { mListener.onClickMovie(position) }
    }

    override fun getItemCount(): Int {
        return listMovie!!.size
    }

//    fun setListData(listData : MutableList<Movie>) : Unit{
//        this.listMovie = listData
//        notifyDataSetChanged()
//
//    }
//    fun getListData(): MutableList<Movie>{
//        return listMovie!!
//    }


}