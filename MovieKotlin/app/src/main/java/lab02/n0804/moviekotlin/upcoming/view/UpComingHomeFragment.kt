package lab02.n0804.moviekotlin.upcoming.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_popular_home.*
import kotlinx.android.synthetic.main.fragment_popular_home.rv_popular
import kotlinx.android.synthetic.main.fragment_up_coming.*
import lab02.n0804.moviekotlin.R
import lab02.n0804.moviekotlin.base.BaseFragment
import lab02.n0804.moviekotlin.main.Movie
import lab02.n0804.moviekotlin.popular.view.PopularHomeFragment
import lab02.n0804.moviekotlin.popular.viewmodel.PopularHomeViewModel
import lab02.n0804.moviekotlin.toprate.viewmodel.TopRateHomeViewModel
import lab02.n0804.moviekotlin.upcoming.adapter.ListMovieHorizontalAdapter
import lab02.n0804.moviekotlin.upcoming.viewmodel.UpComingHomeViewModel

class UpComingHomeFragment : BaseFragment(){
    private var viewModelUpComing: UpComingHomeViewModel? = null
    private var upComingAdapter: ListMovieHorizontalAdapter? = null
    companion object{
        @Volatile var fInstance: UpComingHomeFragment? = null
        fun getInstance() : UpComingHomeFragment {
            if (fInstance == null){
                fInstance = UpComingHomeFragment()
            }
            return fInstance as UpComingHomeFragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_up_coming
    }

    override fun initFragment() {
//        initViewModel()
        initView()
    }

    private fun initView() {

    }

    private fun initViewModel() {
        viewModelUpComing = getViewModel(UpComingHomeViewModel::class.java)
        viewModelUpComing?.initDataViewModel()

        viewModelUpComing?.getListUp()?.observe(this, Observer<List<Movie>?>{ it ->
            if (it != null){
                if (it.isNotEmpty()){
//                    if (upComingAdapter == null){
//                        context?.let { it1 ->
//                            ListMovieHorizontalAdapter(it1,object : ListMovieHorizontalAdapter.ClickItemListener{
//                                override fun onClickMovie(position: Int) {
//
//                                }
//                            })
//                        }
//                        rv_up.adapter = upComingAdapter
//
//                    }else{
//                        rv_up.adapter = upComingAdapter
//                    }
                }
            }else{
                Toast.makeText(getActivity(),"call loi api", Toast.LENGTH_SHORT).show()
            }
        })

    }

}