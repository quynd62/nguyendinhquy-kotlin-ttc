package lab02.n0804.moviekotlin.detailFilm.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetVideoModel(
        @SerializedName("id")
        @Expose
        val id: String,
        @SerializedName("iso_639_1")
        @Expose
        val iso_639_1: String,
        @SerializedName("iso_3166_1")
        @Expose
        val iso_3166_1: String,
        @SerializedName("key")
        @Expose
        val key: String,
        @SerializedName("name")
        @Expose
        val name: String,
        @SerializedName("site")
        @Expose
        val site: String,
        @SerializedName("size")
        @Expose
        val size: Int,
        @SerializedName("type")
        @Expose
        val type: String
)