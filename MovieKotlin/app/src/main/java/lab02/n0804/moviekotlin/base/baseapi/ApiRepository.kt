package lab02.n0804.moviekotlin.base.baseapi

import io.reactivex.Observable
import lab02.n0804.moviekotlin.detailFilm.model.GetVideoResponse
import lab02.n0804.moviekotlin.upcoming.model.MovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiRepository {
    @GET("movie/upcoming")
    fun getUpcomingMovies(@QueryMap params: Map<String,String>): Call<MovieResponse>

    @GET("movie/top_rated")
    fun getTopRatedMovies(@QueryMap params: Map<String,String>): Call<MovieResponse>

    @GET("movie/top_rated?api_key=5fdde824d28c655234d04a2b334c783d")
    fun getTopRatedMoviesNomar(): Call<MovieResponse>

    @GET("movie/popular")
    fun getPopularMovies(@QueryMap params: Map<String,String>): Call<MovieResponse>

    @GET("movie/now_playing")
    fun getNowPlayingMovies(@QueryMap params: Map<String,String>): Call<MovieResponse>

    //RX
    @GET("movie/{movie_id}/videos")
    fun getVideoObs(@Path("movie_id") movieId: String, @QueryMap params: Map<String,String>): Observable<GetVideoResponse>

    @GET("movie/{movie_id}")
    fun getDetailObs(@Path("movie_id") movieId: String, @QueryMap params: Map<String,String>): Observable<GetVideoResponse>

    @GET("movie/{movie_id}/similar")
    fun getSimilarMovie(@Path("movie_id") movieId: String, @QueryMap params: Map<String,String>): Observable<MovieResponse>

    @GET("movie/{movie_id}/recommendations")
    fun getRecommendationMovie(@Path("movie_id") movieId: String, @QueryMap params: Map<String,String>): Observable<MovieResponse>

    @GET("search/movie")
    fun searchFilm(@QueryMap params: Map<String,String>): Call<MovieResponse>

    //coroutines
    @GET("movie/top_rated?api_key=5fdde824d28c655234d04a2b334c783d")
    suspend fun getTopRateCoroutine(): MovieResponse
}