package lab02.n0804.moviekotlin.detailFilm.model

import lab02.n0804.moviekotlin.upcoming.model.MovieResponse

data class AllDetailFilmResponse(
        var getVideoResponse: GetVideoResponse,
        var getDetailResponse: GetDetailResponse,
        var getSimilarResponse: MovieResponse,
        var getRecommendation: MovieResponse
)