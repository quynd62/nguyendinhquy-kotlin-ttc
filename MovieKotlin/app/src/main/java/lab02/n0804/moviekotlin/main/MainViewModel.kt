package lab02.n0804.moviekotlin.main

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io
import lab02.n0804.moviekotlin.util.Utils
import lab02.n0804.moviekotlin.base.BaseViewModel
import lab02.n0804.moviekotlin.base.baseapi.ApiUtil
import lab02.n0804.moviekotlin.upcoming.model.MovieResponse
import org.reactivestreams.Subscriber
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : BaseViewModel(){
    var title :MutableLiveData<String>? = MutableLiveData()
    var listFilmSearch :MutableLiveData<List<Movie>> = MutableLiveData()

    @JvmName("getListFilmSearch1")
    fun getListFilmSearch() : MutableLiveData<List<Movie>>{
        return listFilmSearch
    }


    @JvmName("setListFilmSearch1")
    fun setListFilmSearch(listFilm : MutableLiveData<List<Movie>>) : Unit{
        this.listFilmSearch = listFilm
    }

    @JvmName("getTitle1")
    fun getTitle() : MutableLiveData<String>? = this.title
    @JvmName("setTitle1")
    fun setTitle(newTitle : MutableLiveData<String>) : Unit{
        this.title = newTitle
    }
    fun searchFilm(keyword : String) : Unit{
        var params: HashMap<String, Any?> = HashMap<String,Any?>()
        params.put("api_key",Utils.API_KEY)
        params.put("language","en-US")
        params.put("page","1")
        params.put("include_adult","false")
        params.put("query",keyword)

//        ApiUtil.getDataApi()?.searchFilm(params as Map<String, String>)?.subscribeOn(Schedulers.newThread())?.observeOn(AndroidSchedulers.mainThread()).subscribe(object :
//            Observer<MovieResponse> {
//            override fun onChanged(t: MovieResponse?) {
//                TODO("Not yet implemented")
//            }
//
//
//        })
        var getlistSearch: Call<MovieResponse> = ApiUtil.getDataApi()!!.searchFilm(params as Map<String, String>)
        getlistSearch.enqueue(object : Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                if (response.body() != null && response.body()?.results != null){
                    listFilmSearch.value = response.body()?.results
                }else{
                    Toast.makeText(mActivity,"k co data", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Toast.makeText(mActivity,"call loi", Toast.LENGTH_SHORT).show()
            }

        })

    }
}




