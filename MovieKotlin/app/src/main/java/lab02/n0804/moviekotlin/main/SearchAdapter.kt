package lab02.n0804.moviekotlin.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import lab02.n0804.moviekotlin.R
import lab02.n0804.moviekotlin.util.Utils

class SearchAdapter(private val context: Context,
                    private val mListener:OnClickListener
                    ) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    private var listData:MutableList<Movie>? = null

    inner class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemSearchImg : ImageView
        var titleTv : TextView
        init {
            itemSearchImg = itemView.findViewById(R.id.imv_search)
            titleTv = itemView.findViewById(R.id.tv_item_search)
        }
    }

    interface OnClickListener{
        fun onItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.search_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie: Movie = listData!![position]
        //xử lý cho holder của search adapter, bắt sự kiện,...
        var urlImg:String  = Utils.KEY_IMAGE + movie.posterPath
        Glide.with(holder.itemSearchImg).load(urlImg).into(holder.itemSearchImg)
        holder.titleTv.text = movie.title
        holder.itemSearchImg.setOnClickListener { mListener.onItemClick(position) }
    }

    override fun getItemCount(): Int{
        return listData!!.size
    }

    fun setListData(listData : MutableList<Movie>?) : Unit{
        this.listData = listData
        notifyDataSetChanged()

    }
    fun getListData(): MutableList<Movie>?{
        return listData
    }

}