package lab02.n0804.moviekotlin.detailFilm.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetVideoResponse(
        @SerializedName("id")
        @Expose
        val id: Int,
        @SerializedName("results")
        @Expose
        val results: List<GetVideoModel>
)