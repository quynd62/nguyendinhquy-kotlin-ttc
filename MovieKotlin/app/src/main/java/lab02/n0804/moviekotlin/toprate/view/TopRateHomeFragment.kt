package lab02.n0804.moviekotlin.toprate.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_popular_home.*
import lab02.n0804.moviekotlin.R
import lab02.n0804.moviekotlin.base.BaseFragment
import lab02.n0804.moviekotlin.base.baseapi.ApiUtil
import lab02.n0804.moviekotlin.base.baseapi2.MainCoViewModel
import lab02.n0804.moviekotlin.base.baseapi2.MainCoViewModelFactory
import lab02.n0804.moviekotlin.base.baseapi2.Repository
import lab02.n0804.moviekotlin.main.Movie
import lab02.n0804.moviekotlin.nowplaying.view.NowPlayingHomeFragment
import lab02.n0804.moviekotlin.nowplaying.viewModel.NowPlayingHomeViewModel
import lab02.n0804.moviekotlin.popular.viewmodel.PopularHomeViewModel
import lab02.n0804.moviekotlin.toprate.viewmodel.TopRateHomeViewModel
import lab02.n0804.moviekotlin.upcoming.adapter.ListMovieHorizontalAdapter
import lab02.n0804.moviekotlin.upcoming.model.MovieResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TopRateHomeFragment : BaseFragment() {
    // Su dung corouine de call
    private lateinit var viewModelCo: MainCoViewModel

    private var viewModel: TopRateHomeViewModel? = null
    private var topRateAdapter: ListMovieHorizontalAdapter? = null
    companion object{
        @Volatile var fInstance: TopRateHomeFragment? = null
        fun getInstance() : TopRateHomeFragment {
            if (fInstance == null){
                fInstance = TopRateHomeFragment()
            }
            return fInstance as TopRateHomeFragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_top_rate_home
    }

    override fun initFragment() {
        val repository = Repository()
        val viewModelFactory = MainCoViewModelFactory(repository)
        viewModelCo = ViewModelProvider(this,viewModelFactory).get(MainCoViewModel::class.java)
        viewModelCo.getTopRateCoroutine()
        viewModelCo.myResponse.observe(this, Observer { response ->
            Log.d("quynd",response.totalPages.toString())
            Log.d("quynd",response.totalResults.toString())
            Log.d("quynd",response.page.toString())
            Log.d("quynd",response.results.toString())
            if (response.results != null){
//                if (response.results.isNotEmpty()){
//                    if (topRateAdapter == null){
//                        context?.let { it1 ->
//                            topRateAdapter = ListMovieHorizontalAdapter(it1,
//                                response.results as MutableList<Movie>?,object : ListMovieHorizontalAdapter.ClickItemListener{
//                                override fun onClickMovie(position: Int) {
//
//                                }
//                            })
//                        }
//                        rv_popular.adapter = topRateAdapter
//
//
//
//                    }else{
//                        rv_popular.adapter = topRateAdapter
//                    }
//                }
                topRateAdapter =
                    context?.let {
                        ListMovieHorizontalAdapter(it,response.results,object : ListMovieHorizontalAdapter.ClickItemListener{
                            override fun onClickMovie(position: Int) {

                            }
                        })
                    }
                rv_popular.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
                rv_popular.adapter = topRateAdapter

            }else{
                Toast.makeText(getActivity(),"call loi api", Toast.LENGTH_SHORT).show()
            }
        })
//        initViewModel()
        initView()
//        val getListTopNor : Call<MovieResponse> = ApiUtil.getDataApi()!!.getTopRatedMoviesNomar()
//        getListTopNor.enqueue(object : Callback<MovieResponse> {
//            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
//                if (response.isSuccessful){
//                    Toast.makeText(getActivity(),"call thanh cong", Toast.LENGTH_SHORT).show()
////                    if (response.body()?.listMovie != null){
////                        if (response.body()?.listMovie!!.isNotEmpty()){
////                            if (topRateAdapter == null){
////                                context?.let { it1 ->
////                                    ListMovieHorizontalAdapter(it1,object : ListMovieHorizontalAdapter.ClickItemListener{
////                                        override fun onClickMovie(position: Int) {
////
////                                        }
////                                    })
////                                }
////                                rv_popular.adapter = topRateAdapter
////
////                            }else{
////                                rv_popular.adapter = topRateAdapter
////                            }
////                        }
////
////                    }else{
////                        Toast.makeText(getActivity(),"list null", Toast.LENGTH_SHORT).show()
////                    }
//                }
//            }
//
//            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
//            }
//
//        })
    }

    private fun initView() {

    }

    private fun initViewModel() {
        viewModel = getViewModel(TopRateHomeViewModel::class.java)
        viewModel?.initDataViewModel()

        viewModel?.getListTop()?.observe(this, Observer<List<Movie>?>{ it ->
            if (it != null){
                if (it.isNotEmpty()){
//                    if (topRateAdapter == null){
//                        context?.let { it1 ->
//                            ListMovieHorizontalAdapter(it1,object : ListMovieHorizontalAdapter.ClickItemListener{
//                                override fun onClickMovie(position: Int) {
//
//                                }
//                            })
//                        }
//                        rv_popular.adapter = topRateAdapter
//
//                    }else{
//                        rv_popular.adapter = topRateAdapter
//                    }
                }
            }else{
                Toast.makeText(getActivity(),"call loi api", Toast.LENGTH_SHORT).show()
            }
        })

    }

}