package lab02.n0804.moviekotlin.detailFilm.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class ProductionCompany(
        @SerializedName("id")
        @Expose
        val id:Int,
        @SerializedName("logo_path")
        @Expose
        val logo_path:Objects,
        @SerializedName("name")
        @Expose
        val name:String,
        @SerializedName("origin_country")
        @Expose
        val origin_country:String
)