package lab02.n0804.moviekotlin.base.baseapi2

import lab02.n0804.moviekotlin.base.baseapi.ApiRepository
import lab02.n0804.moviekotlin.base.baseapi.ApiUtil.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstanece {
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val api: ApiRepository by lazy {
        retrofit.create(ApiRepository::class.java)
    }
}