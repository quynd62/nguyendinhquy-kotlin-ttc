package lab02.n0804.moviekotlin.base

import android.annotation.SuppressLint
import android.app.Activity
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    protected var mActivity:Activity? = null

    fun setActivity(mActivity : Activity): BaseViewModel{
        this.mActivity = mActivity
        return this
    }
}