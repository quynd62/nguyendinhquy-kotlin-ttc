package lab02.n0804.moviekotlin.upcoming.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import lab02.n0804.moviekotlin.main.Movie

data class MovieResponse(
        @SerializedName("page")
        val page: Int? = 0,
        @SerializedName("results")
        val results: List<Movie>? = listOf(),
        @SerializedName("total_pages")
        val totalPages: Int? = 0,
        @SerializedName("total_results")
        val totalResults: Int? = 0
)