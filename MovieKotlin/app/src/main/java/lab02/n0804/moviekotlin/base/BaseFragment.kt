package lab02.n0804.moviekotlin.base

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import butterknife.ButterKnife

abstract class BaseFragment : Fragment() {
    protected var activity : Activity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootview:View? = inflater.inflate(getLayoutId(),container,false)
        rootview?.let { ButterKnife.bind(this, it) }
        return rootview
    }

    abstract fun getLayoutId(): Int


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initFragment()
    }

    abstract fun initFragment()

    protected fun onBackFragment(): Unit{
        activity?.onBackPressed()
    }

    fun <T : BaseViewModel> getViewModel(viewmodel : Class<T>) : T? {
        var factory : ViewModelProvider.Factory? = ViewModelProvider.NewInstanceFactory()
        var vm:T? =  ViewModelProvider(this,factory!!).get(viewmodel)
        vm?.setActivity(activity!!)
        return vm
    }

}