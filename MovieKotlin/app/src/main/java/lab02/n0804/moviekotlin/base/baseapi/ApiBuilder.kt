package lab02.n0804.moviekotlin.base.baseapi

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

class ApiBuilder {
    companion object{
        fun getApi(base_url : String) :Retrofit?{
            var okHttpClient: OkHttpClient? = OkHttpClient.Builder()
                    .readTimeout(10000,TimeUnit.MILLISECONDS)
                    .writeTimeout(10000,TimeUnit.MILLISECONDS)
                    .connectTimeout(10000,TimeUnit.MILLISECONDS)
                    .retryOnConnectionFailure(true)
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .protocols(listOf(Protocol.HTTP_1_1))
                    .build()

            var gson : Gson? = GsonBuilder().setLenient().create()
            var retrofit: Retrofit? = Retrofit.Builder()
                    .baseUrl(base_url)
                    .client(okHttpClient!!)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit

        }
    }
}

