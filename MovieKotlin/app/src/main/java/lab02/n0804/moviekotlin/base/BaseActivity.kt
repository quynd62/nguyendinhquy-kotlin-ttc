package lab02.n0804.moviekotlin.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import butterknife.ButterKnife
import lab02.n0804.moviekotlin.R

abstract class BaseActivity : AppCompatActivity() {
    private var transaction: FragmentTransaction? = null

    protected override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        ButterKnife.bind(this)
        transaction = supportFragmentManager.beginTransaction()
        initActivity()
    }

    abstract fun initActivity()

    open protected fun getLayoutId(): Int {
        return R.layout.layout_container
    }

    protected fun pushView(fragment: Fragment): Unit {
        transaction?.add(R.id.container_frame, fragment)
        transaction?.commitAllowingStateLoss()
    }

    fun <T : BaseViewModel> getViewModel(viewmodel: Class<T>): T? {
        var factory: ViewModelProvider.Factory? = ViewModelProvider.NewInstanceFactory()
        var vm: T? = ViewModelProvider(this, factory!!).get(viewmodel)
        vm?.setActivity(this)
        return vm
    }



}