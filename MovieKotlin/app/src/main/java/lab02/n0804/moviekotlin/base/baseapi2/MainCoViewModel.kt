package lab02.n0804.moviekotlin.base.baseapi2

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import lab02.n0804.moviekotlin.upcoming.model.MovieResponse

class MainCoViewModel(private val repository: Repository) : ViewModel(){
    val myResponse: MutableLiveData<MovieResponse> = MutableLiveData()

    fun getTopRateCoroutine(){
        viewModelScope.launch {
            val response:MovieResponse = repository.getTopRateCoroutine()
            myResponse.value = response
        }
    }
}