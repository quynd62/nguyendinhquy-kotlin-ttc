package lab02.n0804.moviekotlin.popular.viewmodel

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import lab02.n0804.moviekotlin.base.BaseViewModel
import lab02.n0804.moviekotlin.base.baseapi.ApiUtil
import lab02.n0804.moviekotlin.main.Movie
import lab02.n0804.moviekotlin.upcoming.model.MovieResponse
import lab02.n0804.moviekotlin.util.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PopularHomeViewModel : BaseViewModel() {
    private var listPopularMovie : MutableLiveData<List<Movie>> = MutableLiveData()
    private var isCallApi : MutableLiveData<Boolean> = MutableLiveData()

    fun getIsCallApi() : MutableLiveData<Boolean> {
        return isCallApi
    }

    fun setIsCallApi(isCallApi: MutableLiveData<Boolean>) : Unit{
        this.isCallApi = isCallApi
    }

    fun getListPop() : MutableLiveData<List<Movie>> {
        return listPopularMovie
    }

    fun setListPop(listPop : MutableLiveData<List<Movie>>) : Unit{
        this.listPopularMovie = listPop
    }

    fun initDataViewModel() : Unit{
        getListPoPular()
    }

    private fun getListPoPular() {
        //call Api để láy về dữ liệu
        var params : HashMap<String,Any> = hashMapOf<String,Any>()
        params.put("api_key", Utils.API_KEY)

        var getlistPopMovie: Call<MovieResponse> = ApiUtil.getDataApi()!!.getPopularMovies(params as Map<String, String>)
        getlistPopMovie.enqueue(object : Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                if (response.body() != null && response.body()?.results != null){
                    listPopularMovie.value = response.body()?.results
                }else{
                    Toast.makeText(mActivity,"k co data", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Toast.makeText(mActivity,"call loi", Toast.LENGTH_SHORT).show()
            }

        })
    }
}